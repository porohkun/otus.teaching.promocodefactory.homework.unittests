﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;

namespace AutoFixture
{
    public static class FixtureExtensions
    {
        public static Partner BuildPartner(this Fixture fixture, bool isActive = true, bool haveActiveLimits = true, bool haveInactiveLimits = true)
        {
            var limits = new List<PartnerPromoCodeLimit>();
            if (haveActiveLimits)
                limits.AddRange(new[]
                {
                    fixture.Build<PartnerPromoCodeLimit>()
                        .Without(l => l.Partner)
                        .Without(l => l.CancelDate)
                        .With(l => l.EndDate, DateTime.UtcNow.AddDays(1))
                        .Create(),
                    fixture.Build<PartnerPromoCodeLimit>()
                        .Without(l => l.Partner)
                        .Without(l => l.CancelDate)
                        .With(l => l.EndDate, DateTime.UtcNow.AddDays(1))
                        .Create()
                });
            if (haveInactiveLimits)
                limits.AddRange(new[]
                {
                    fixture.Build<PartnerPromoCodeLimit>()
                        .Without(l => l.Partner)
                        .With(l => l.CancelDate, DateTime.UtcNow.AddDays(-1))
                        .Create(),
                    fixture.Build<PartnerPromoCodeLimit>()
                        .Without(l => l.Partner)
                        .With(l => l.CancelDate, DateTime.UtcNow.AddDays(-1))
                        .Create()
                });

            return fixture.Build<Partner>()
                .With(p => p.IsActive, isActive)
                .With(p => p.PartnerLimits, limits)
                .Create();
        }

        public static SetPartnerPromoCodeLimitRequest BuildSetPartnerPromoCodeLimitRequest(this Fixture fixture,
            int limit = 10, int daysAfterNow = 1)
        {
            return fixture.Build<SetPartnerPromoCodeLimitRequest>()
               .With(r => r.Limit, limit)
               .With(r => r.EndDate, DateTime.UtcNow.AddDays(daysAfterNow))
               .Create();
        }
    }
}
