﻿using AutoFixture;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using System;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests
    {
        readonly Mock<IRepository<Partner>> _repositoryMockedData;
        readonly PartnersController _controller;

        public SetPartnerPromoCodeLimitAsyncTests()
        {
            _repositoryMockedData = new Mock<IRepository<Partner>>();
            _controller = new PartnersController(_repositoryMockedData.Object);
        }

        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_PartnerIsNotFound_ReturnsNotFound()
        {
            // Arrange
            var request = new Fixture().BuildSetPartnerPromoCodeLimitRequest();

            // Act
            var result = await _controller.SetPartnerPromoCodeLimitAsync(Guid.NewGuid(), request);

            // Assert
            result.Should().BeOfType<NotFoundResult>();
        }

        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_PartnerIsBlocked_ReturnsBadRequest()
        {
            // Arrange
            var request = new Fixture().BuildSetPartnerPromoCodeLimitRequest();
            var partner = new Fixture().BuildPartner(isActive: false);
            _repositoryMockedData.Setup(d => d.GetByIdAsync(partner.Id)).ReturnsAsync(partner);

            // Act
            var result = await _controller.SetPartnerPromoCodeLimitAsync(partner.Id, request);

            // Assert
            result.Should().BeOfType<BadRequestObjectResult>();
        }

        [Theory]
        [InlineData(0)]
        [InlineData(-5)]
        public async Task SetPartnerPromoCodeLimitAsync_RequestLimitLessOrEqThan0_ReturnsBadRequest(int limit)
        {
            // Arrange
            var request = new Fixture().BuildSetPartnerPromoCodeLimitRequest(limit: limit);

            // Act
            var result = await _controller.SetPartnerPromoCodeLimitAsync(Guid.NewGuid(), request);

            // Assert
            result.Should().BeOfType<BadRequestObjectResult>();
        }

        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        public async Task SetPartnerPromoCodeLimitAsync_RequestHasDateFromPast_ReturnsBadRequest(int addDays)
        {
            // Arrange
            var request = new Fixture().BuildSetPartnerPromoCodeLimitRequest(daysAfterNow: addDays);

            // Act
            var result = await _controller.SetPartnerPromoCodeLimitAsync(Guid.NewGuid(), request);

            // Assert
            result.Should().BeOfType<BadRequestObjectResult>();
        }

        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_WhenActiveLimitExists_NumberIssuedPromoCodesIs0()
        {
            // Arrange
            var request = new Fixture().BuildSetPartnerPromoCodeLimitRequest();
            var partner = new Fixture().BuildPartner();
            _repositoryMockedData.Setup(d => d.GetByIdAsync(partner.Id)).ReturnsAsync(partner);
            _repositoryMockedData.Setup(d => d.UpdateAsync(It.IsAny<Partner>())).Callback<Partner>(r => partner = r);

            // Act
            var result = await _controller.SetPartnerPromoCodeLimitAsync(partner.Id, request);

            // Assert
            partner.NumberIssuedPromoCodes.Should().Be(0);
        }

        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_WhenActiveLimitExpired_NumberIssuedPromoCodesIsTheSame()
        {
            // Arrange
            var request = new Fixture().BuildSetPartnerPromoCodeLimitRequest();
            var partner = new Fixture().BuildPartner(haveActiveLimits: false);
            var partnerNumberIssuedPromoCodes = partner.NumberIssuedPromoCodes;
            _repositoryMockedData.Setup(d => d.GetByIdAsync(partner.Id)).ReturnsAsync(partner);
            _repositoryMockedData.Setup(d => d.UpdateAsync(It.IsAny<Partner>())).Callback<Partner>(r => partner = r);

            // Act
            var result = await _controller.SetPartnerPromoCodeLimitAsync(partner.Id, request);

            // Assert
            partner.NumberIssuedPromoCodes.Should().Be(partnerNumberIssuedPromoCodes);
        }

        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_WhenNewLimit_OtherLimitsMustBeDeactivated()
        {
            // Arrange
            var request = new Fixture().BuildSetPartnerPromoCodeLimitRequest();
            var partner = new Fixture().BuildPartner();
            _repositoryMockedData.Setup(d => d.GetByIdAsync(partner.Id)).ReturnsAsync(partner);
            _repositoryMockedData.Setup(d => d.UpdateAsync(It.IsAny<Partner>())).Callback<Partner>(r => partner = r);

            // Act
            var result = await _controller.SetPartnerPromoCodeLimitAsync(partner.Id, request);

            // Assert
            partner.PartnerLimits.Count(l => l.IsActive).Should().Be(1);
        }

        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_WhenNewLimitAdded_NewLimitAdded()
        {
            // Arrange
            var request = new Fixture().BuildSetPartnerPromoCodeLimitRequest();
            var partner = new Fixture().BuildPartner();
            var partnerId = partner.Id;
            _repositoryMockedData.Setup(d => d.GetByIdAsync(partner.Id)).ReturnsAsync(partner);
            _repositoryMockedData.Setup(d => d.UpdateAsync(It.IsAny<Partner>())).Callback<Partner>(r => partner = r);

            // Act
            var result = await _controller.SetPartnerPromoCodeLimitAsync(partner.Id, request);

            // Assert
            result.Should().BeOfType<CreatedAtActionResult>();

            var newLimit = partner.PartnerLimits.First(l => l.IsActive);
            newLimit.PartnerId.Should().Be(partnerId);
            newLimit.CreateDate.Should().BeOnOrBefore(DateTime.UtcNow);
            newLimit.CancelDate.Should().BeNull();
            newLimit.EndDate.Should().Be(request.EndDate);
            newLimit.Limit.Should().Be(request.Limit);
        }
    }
}