﻿using AutoFixture;
using FluentAssertions;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class PartnerPromoCodeLimitTests
    {
        [Theory]
        [InlineData(-1)]
        [InlineData(0)]
        [InlineData(1)]
        public void PartnerPromoCodeLimitTests_HaveAnyCancelDate_IsActiveIsFalse(int addDays)
        {
            // Arrange
            var limit = new Fixture().Build<PartnerPromoCodeLimit>()
                .Without(l => l.Partner)
                .With(l => l.CancelDate, DateTime.UtcNow.AddDays(addDays))
                .With(l => l.EndDate, DateTime.UtcNow.AddDays(1))
                .Create();

            // Act
            var result = limit.IsActive;

            // Assert
            result.Should().BeFalse();
        }

        [Fact]
        public void PartnerPromoCodeLimitTests_EndDateLessThanNow_IsActiveIsFalse()
        {
            // Arrange
            var limit = new Fixture().Build<PartnerPromoCodeLimit>()
                .Without(l => l.Partner)
                .Without(l => l.CancelDate)
                .With(l => l.EndDate, DateTime.UtcNow.AddDays(-1))
                .Create();

            // Act
            var result = limit.IsActive;

            // Assert
            result.Should().BeFalse();
        }

        [Fact]
        public void PartnerPromoCodeLimitTests_NoCancelDateIsAndEndDateMoreThanNow_IsActiveIsTrue()
        {
            // Arrange
            var limit = new Fixture().Build<PartnerPromoCodeLimit>()
                .Without(l => l.Partner)
                .Without(l => l.CancelDate)
                .With(l => l.EndDate, DateTime.UtcNow.AddDays(1))
                .Create();

            // Act
            var result = limit.IsActive;

            // Assert
            result.Should().BeTrue();
        }

        [Fact]
        public void PartnerPromoCodeLimitTests_DeactivateCall_IsActiveIsTrue()
        {
            // Arrange
            var limit = new Fixture().Build<PartnerPromoCodeLimit>()
                .Without(l => l.Partner)
                .Without(l => l.CancelDate)
                .With(l => l.EndDate, DateTime.UtcNow.AddDays(1))
                .Create();

            // Act
            limit.Deactivate();

            // Assert
            limit.IsActive.Should().BeFalse();
        }

    }
}